////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef TIMERMANAGER_HPP
#define TIMERMANAGER_HPP

////////////////////////////////////////////////////////////
#include <SFML/System.hpp>
#include <map>
#include <string>
#include <ZGE/Signal.hpp>
#include <ZGE/Identifiable.hpp>

namespace ZGE
{

class TimerManager;

////////////////////////////////////////////////////////////
class Timer : public Identifiable
{
public:

	Timer() = delete;
	Timer(const Timer& copy) = delete;
	Timer(const sf::Time& time, bool started, bool repeated);
	Timer(const sf::Time& time, bool started, bool repeated, std::function<void()> function);
	void start();
	void pause();
	void restart();
	void stop();

	void update();

	Signal<> signal;

protected:

	friend class TimerManager;
	sf::Time m_currentTime, m_timeToReach;
	bool m_started, m_repeated;
};

////////////////////////////////////////////////////////////
class TimerManager
{
public:

	TimerManager() = delete;
	TimerManager(const TimerManager& timeManager) = delete;

	static bool erase(size_t id);
   	static void clear();
   	template <typename Object, typename Type>
	static size_t create(const sf::Time& time, bool started, bool repeated, Object* object, void(Type::*slot)());
	static size_t create(const sf::Time& time, bool started, bool repeated, std::function<void()> function);
	static size_t create(const sf::Time& time, bool started, bool repeated);
	static Timer& get(size_t id);
	static void pause(bool enabled);
	static void startTimer(size_t id);
	static void pauseTimer(size_t id);
	static void restartTimer(size_t id);
	static void stopTimer(size_t id);
	static void update();

protected:
	
	static std::map<size_t, Timer*> m_timers;
	static bool m_pause;
	static sf::Clock m_clock;
};

////////////////////////////////////////////////////////////
template <typename Object, typename Type>
size_t TimerManager::create(const sf::Time& time, bool started, bool repeated, Object* object, void(Type::*slot)())
{
	Timer* timer = new Timer(time, started, repeated);
	timer->signal.connect(object, slot);
	size_t id = timer->id;
	m_timers[id] = timer;
	return id;
}

}

#endif // TIMERMANAGER_HPP