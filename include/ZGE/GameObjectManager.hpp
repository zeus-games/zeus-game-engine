////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef ZGE_GAME_OBJECT_MANAGER
#define ZGE_GAME_OBJECT_MANAGER

#include <ZGE/GameObject.hpp>
#include <ZGE/Identifiable.hpp>
#include <map>

namespace ZGE
{

////////////////////////////////////////////////////////////
class GameObjectManager
{
public:

	GameObjectManager() = delete;
	GameObjectManager(const GameObjectManager& gameObjectManager) = delete;
	~GameObjectManager() = delete;

	template <typename Type, typename... Args>
	static size_t create(Args... args)
	{
		static_assert(check<Type>(), "Forbidden template type !");
		GameObject* game_object_ptr = new Type(args...);
		size_t id = game_object_ptr->id;
	    m_gameObjects[id] = game_object_ptr;
	    return id;
	}

	template<typename Type>
    static Type& get(size_t id)
    {
    	static_assert(check<Type>(), "Forbidden template type !");
    	auto it = m_gameObjects.find(id);
        assert(it != m_gameObjects.end());
        return *reinterpret_cast<Type*>(it->second);
    }

	static bool erase(size_t);
	static void clear();

protected:

	template <typename Type>
	static constexpr bool check()
	{
		return !std::is_base_of<Type, GameObject>::value;
	}

	static std::map<size_t, GameObject*> m_gameObjects;
};

}

#endif // ZGE_GAME_OBJECT_MANAGER