////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef ZGE_WIDGET_MANAGER
#define ZGE_WIDGET_MANAGER

#include <ZGE/Reachable.hpp>
#include <ZGE/Identifiable.hpp>
#include <ZGE/Updatable.hpp>
#include <ZGE/EventManager.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <cassert>

namespace ZGE
{

enum Alignment
{
    Default, Left, Middle, Right, Top, Bottom, 
    LeftTop, LeftMiddle, LeftBottom,
    MiddleTop, MiddleMiddle, MiddleBottom,
    RightTop, RightMiddle, RightBottom
};

////////////////////////////////////////////////////////////
class Widget : public Identifiable, public Updatable
{
public:

    typedef Widget* ptr;

    Widget();
   ~Widget();

    Widget& init();
    Widget& attach(Widget& widget);
    Widget& detach(Widget& widget);
    Widget& focus();
    Widget& unfocus();
    Widget& show();
    Widget& hide();
    Widget& lock();
    Widget& unlock();
    Widget& activate();
    Widget& deactivate();
    Widget& setPosition(const sf::Vector2f& pos);
    Widget& setPosition(float pos_x, float pos_y);
    Widget& move(const sf::Vector2f& offset);
    Widget& move(float offset_x, float offset_y);
    Widget& align(const sf::FloatRect& limits, Alignment alignment);
    Widget& setSize(const sf::Vector2f& size);

    const sf::Vector2f& getSize();

    void focusPrevious();
    void focusNext();

protected:

    friend class WidgetManager;

    virtual void onInit() {}
    virtual void onUpdate() {}
    virtual void onWindowResized(sf::Uint32 width, sf::Uint32 height) {}
    virtual void onKeyPressed(size_t key) {}
    virtual void onTextEntered(size_t unicode) {}
    virtual void onMouseMoved(float x, float y) {}
    virtual void onLock() {}
    virtual void onUnlock() {}
    virtual void onFocus() {}
    virtual void onUnfocus() {}
    virtual void onMove() {}
    virtual void onResizing() {}

    bool m_visible = true, m_locked = false, m_focused = false;
    sf::Vector2f m_pos, m_size;
    std::vector<Widget::ptr> m_children, m_parents;
    size_t m_focused_child = 0, m_graph_id;
};

////////////////////////////////////////////////////////////
class MenuItem : public Widget
{
public:

    MenuItem(const sf::String& label);

    const sf::Vector2f& getSize();

protected:

    virtual void onFocus();
    virtual void onUnfocus();

    size_t m_label_id;
};

////////////////////////////////////////////////////////////
class MenuItemValidation : public MenuItem
{
public:

    MenuItemValidation(const sf::String& label);

    MenuItemValidation& connect(std::function<void()> slot);
    template <typename Object, typename Type>
    MenuItemValidation& connect(Object* object, void(Type::*slot)());

protected:

    void onKeyPressed(size_t key) {}
    void onFocus();
    void onUnfocus();
    void onValidation();

    Signal<> m_validationSignal;
    size_t m_return_key_pressed_id;
};

template <typename Object, typename Type>
MenuItemValidation& MenuItemValidation::connect(Object* object, void(Type::*slot)())
{
    m_validationSignal.connect(object, slot);
    return *this;
}

////////////////////////////////////////////////////////////
class MenuItemSelection : public MenuItem
{
public:

    MenuItemSelection(const sf::String& label, const sf::String& selection);

protected:

    void onKeyPressed(size_t key) {}
};

////////////////////////////////////////////////////////////
class MenuItemEdition : public MenuItem
{
public:

    MenuItemEdition(const sf::String& label, const sf::String& def = "");
   ~MenuItemEdition();

    MenuItemEdition& connect(std::function<void()> function);
    template <typename Object, typename Type>
    MenuItemEdition& connect(Object* object, void(Type::*slot)());

protected:

    void onTextEntered(size_t unicode);
    void onFocus();
    void onUnfocus();

    size_t m_text_id, m_text_entered_event;
    Signal<> m_edition_signal;
};

////////////////////////////////////////////////////////////
template <typename Object, typename Type>
MenuItemEdition& MenuItemEdition::connect(Object* object, void(Type::*slot)())
{
    m_edition_signal.connect(object, slot);
    return *this;
}

////////////////////////////////////////////////////////////
class Menu : public Widget
{
public:

    Menu(const sf::String& title, Alignment slot_alignment = Alignment::Default, float line_spacing = 10);
   ~Menu();

    Menu& attach(size_t slot_id);

protected:

    void onLock();
    void onUnlock();
    void onInit();

    size_t m_up_key_pressed_event, m_down_key_pressed_event, m_back_shape;
    std::vector<MenuItem*> m_slots;
    Alignment m_alignment;
    float m_line_spacing;
};

////////////////////////////////////////////////////////////
class WidgetManager
{
public:

    WidgetManager() = delete;
    WidgetManager(const WidgetManager& copy) = delete;
   ~WidgetManager() = delete;

    template <typename Type, typename... Args>
    static Type& create(Args... args);
    template<typename Type>
    static Type& get(size_t id);
    static bool erase(size_t);
    static void clear();
    static void attach(size_t id);
    static void detach(size_t id);

protected:

    template <typename Type>
    static constexpr bool check();

    static std::map<size_t, Widget::ptr> m_widgets;
    static std::vector<Widget*> m_attached_widgets;
};

////////////////////////////////////////////////////////////
template <typename Type, typename... Args>
Type& WidgetManager::create(Args... args)
{
    static_assert(check<Type>(), "Forbidden template type !");
    Type* widget = new Type(args...);
    size_t id = widget->id;
    m_widgets[id] = widget;
    return *widget;
}

////////////////////////////////////////////////////////////
template<typename Type>
Type& WidgetManager::get(size_t id)
{
    static_assert(check<Type>(), "Forbidden template type !");
    auto it = m_widgets.find(id);
    return *reinterpret_cast<Type*>(it->second);
}

////////////////////////////////////////////////////////////
template <typename Type>
constexpr bool WidgetManager::check()
{
    return !std::is_base_of<Type, Widget>::value || std::is_same<Type, Widget>::value;
}

}

#endif // ZGE_WIDGET_MANAGER