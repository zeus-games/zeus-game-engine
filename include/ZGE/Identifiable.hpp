////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef ZGE_IDENTIFIABLE
#define ZGE_IDENTIFIABLE

#include <cstddef>

namespace ZGE
{

class Identifiable
{
public:
	
	size_t id;
	Identifiable() : id(create()) {}
	identifiable(const Identifiable& copy) = delete;
	operator size_t() { return id; }

private:

	size_t create()
	{
		static size_t count;
		return count++;
	}
};

}

#endif // ZGE_IDENTIFIABLE