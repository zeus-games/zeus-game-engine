////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#include <ZGE/Reachable.hpp>
#include <iostream>

namespace ZGE
{

////////////////////////////////////////////////////////////
template<typename... Args>
Signal<Args...>::Signal() : m_functions(), m_slots() {}

////////////////////////////////////////////////////////////
template<typename... Args>
Signal<Args...>::~Signal()
{
    disconnect();
}

////////////////////////////////////////////////////////////
template<typename... Args>
void Signal<Args...>::connect(Function function)
{
    if( function )
        m_functions.push_back(function);
}

////////////////////////////////////////////////////////////
template<typename... Args>
template<typename Object, typename Type, typename... TypeArgs>
void Signal<Args...>::connect(Object* object_t, void (Type::*slot)(TypeArgs...))
{
    Reachable* object = reinterpret_cast<Reachable*>(object_t);
    std::function<void(Type*, TypeArgs...)> slot_t = slot;
    
    if( object && slot_t )
    {
        m_slots[object].push_back(*reinterpret_cast<Slot*>(&slot_t));
        object_t->m_links.insert(this);
    }
}

////////////////////////////////////////////////////////////
template<typename... Args>
void Signal<Args...>::operator()(Args... arg)
{
    for( auto it = m_functions.begin() ; it != m_functions.end() ; ++it )
        (*it)(arg...);

    for( auto ik = m_slots.begin() ; ik != m_slots.end() ; ++ik )
        for( auto it = ik->second.begin() ; it != ik->second.end() ; ++it )
            (*it)(ik->first, arg...);
}

////////////////////////////////////////////////////////////
template<typename... Args>
void Signal<Args...>::call(Args... arg)
{
    for( auto it = m_functions.begin() ; it != m_functions.end() ; ++it )
        (*it)(arg...);

    for( auto ik = m_slots.begin() ; ik != m_slots.end() ; ++ik )
        for( auto it = ik->second.begin() ; it != ik->second.end() ; ++it )
            (*it)(ik->first, arg...);
}

////////////////////////////////////////////////////////////
template<typename... Args>
void Signal<Args...>::disconnect()
{
    m_functions.clear();

    for( auto ik = m_slots.begin(); ik != m_slots.end(); ++ik )
        ik->first->m_links.erase(this);

    m_slots.clear();
}

////////////////////////////////////////////////////////////
template<typename... Args>
void Signal<Args...>::disconnect(Reachable* object)
{
    if( object != nullptr )
    {
        object->m_links.erase(this);
        m_slots.erase(object);
    }
}

////////////////////////////////////////////////////////////
template<typename... Args>
void Signal<Args...>::_disconnect(Reachable* object)
{
    m_slots.erase(object);
}

}