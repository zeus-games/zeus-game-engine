////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef GRAPHICS_HPP
#define GRAPHICS_HPP

////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <map>
#include <cassert>
#include <iostream>

namespace ZGE
{

////////////////////////////////////////////////////////////
class Graph : public sf::Drawable, public sf::Transformable
{
public:

	Graph(bool showed = true);
	Graph& attach(size_t id);
	Graph& detach(size_t id);
	Graph& setDepth(size_t id, size_t range);

	bool showed = true;

protected:

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	std::vector<sf::Drawable*>::iterator find(sf::Drawable* drawable_);

	std::vector<sf::Drawable*> m_drawables;
};

////////////////////////////////////////////////////////////
class GraphicManager
{
public:

	GraphicManager() = delete;
	GraphicManager(const GraphicManager& graphicManager) = delete;

	static void render(sf::RenderTarget& target);
	static void clear();
	static bool erase(size_t id);

	template<typename Type, typename... Arguments>
   	static size_t create(Arguments... arguments)
   	{
   		static_assert(check<Type>(), "GraphicManager does not support this type !");
        Type* ptr = new Type(arguments...);
        m_drawables[count] = ptr;
        return count++;
   	}

   	template<typename Type>
    static Type& get(size_t id)
    {
    	static_assert(check<Type>(), "GraphicManager does not support this type !");
    	auto it = m_drawables.find(id);
        assert(it != m_drawables.end());
        return *reinterpret_cast<Type*>(it->second);
    }

    static void setDefaultFontPath(const sf::String& path_str);

	static size_t createSprite(size_t resource_id, const sf::Vector2f& pos = sf::Vector2f(0, 0));
	static size_t createText(size_t resource_id);
	static size_t createText();

	static void attach(size_t id);
	static void detach(size_t id);

	static void rotate(size_t id, float angle);
	static void move(size_t id, float x, float y);
	static float getRotation(size_t id);
	static const sf::Vector2f& getPosition(size_t id);
	static void setOrigin(size_t id, float x, float y);
	static void setRotation(size_t id, float angle);
	static void setScale(size_t id, float x, float y);
	static void setTexture(size_t id, size_t texture_id);
	static float getSpriteWidth(size_t id);
	static float getSpriteHeight(size_t id);
	static void center(size_t id);

protected:
	
	template<typename Type>
    static constexpr bool check()
    {
      	return !std::is_base_of<Type, sf::Drawable>::value || std::is_same<Type, sf::Drawable>::value;
    }

	static Graph m_graph;
	static std::map<size_t, sf::Drawable*> m_drawables;
	static size_t count;
	static size_t m_default_font_id;
};

}

#endif // GRAPHICS_HPP