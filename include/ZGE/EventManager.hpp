////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef ZGE_EVENT_MANAGER
#define ZGE_EVENT_MANAGER

#include <map>
#include <vector>
#include <array>
#include <ZGE/Signal.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/System/Time.hpp>
#include <ZGE/Identifiable.hpp>

namespace ZGE
{

class EventManager;
class TimerManager;

////////////////////////////////////////////////////////////
class Event : public Reachable, public Identifiable
{
public:

	Event();
	Event(const sf::Event& event) = delete;
	virtual ~Event();

	void trigger();
	void listen(const sf::Event& sf_event);
	void attach(Event& child);
	void detach(Event& child);
	void enable();
	void disable();

	template <typename  Object, typename Type>
	void connect(Object* object, void(Type::*slot)());
	template <typename Object, typename Type>
	void connect(Object* object, void(Type::*slot)(size_t));
	template <typename Object, typename Type>
	void connect(Object* object, void(Type::*slot)(float, float));
	void connect(void(*slot)());
	void connect(void(*slot)(size_t));
	void connect(void(*slot)(float, float));

	bool repeated = false;
	
protected:

	friend class EventManager;
		
	virtual bool find(const sf::Event& sf_event) = 0;

	bool m_triggered = false, m_enabled = true;
	std::vector<Event*> m_children, m_parents;
	Signal<> m_signal;
	Signal<size_t> m_signal_uint;
	size_t m_uint;
	Signal<float, float> m_signal_2f;
 	std::array<float, 2> m_2f;
};

////////////////////////////////////////////////////////////
template <typename Object, typename Type>
void Event::connect(Object* object, void(Type::*slot)())
{
	m_signal.connect(object, slot);
}

////////////////////////////////////////////////////////////
template <typename Object, typename Type>
void Event::connect(Object* object, void(Type::*slot)(size_t))
{
	m_signal_uint.connect(object, slot);
}

////////////////////////////////////////////////////////////
template <typename Object, typename Type>
void Event::connect(Object* object, void(Type::*slot)(float, float))
{
	m_signal_2f.connect(object, slot);
}

////////////////////////////////////////////////////////////
class TimeOut : public Event
{
public:

	TimeOut(sf::Time time);

protected:

	~TimeOut();
	bool find(const sf::Event& event);
	void reset();
	void enable();
	void disable();

	size_t m_timer_id;
};

////////////////////////////////////////////////////////////
class KeyPressed : public Event
{
public:

	KeyPressed();
	KeyPressed(size_t key);
	bool find(const sf::Event& event);

protected:

	bool m_check;
	size_t m_key;
};

////////////////////////////////////////////////////////////
class KeyReleased : public Event
{
public:

	KeyReleased();
	KeyReleased(size_t key);
	bool find(const sf::Event& event);

protected:

	bool m_check = false;
	size_t m_key;
};

////////////////////////////////////////////////////////////
class TextEntered : public Event
{
public:

	TextEntered();
	bool find(const sf::Event& event);
};

////////////////////////////////////////////////////////////
class MouseMoved : public Event
{
public:

	MouseMoved();
	bool find(const sf::Event& event);
};

////////////////////////////////////////////////////////////
class MouseButtonPressed : public Event
{
public:

	MouseButtonPressed(size_t button);
	bool find(const sf::Event& event);

protected:

	size_t m_button;
};

////////////////////////////////////////////////////////////
class MouseButtonReleased : public Event
{
public:

	MouseButtonReleased(size_t button);
	bool find(const sf::Event& event);

protected:

	size_t m_button;
};

////////////////////////////////////////////////////////////
class EventManager
{
public:

	EventManager() = delete;
	EventManager(const sf::Event& event) = delete;

	template <typename Type, typename... Args>
	static size_t create(Args... args);
	static bool erase(size_t id);
	static Event& get(size_t id);
	static void clear();
	static void attach(size_t id, bool repeated = false);
	static void attach(size_t parent_id, size_t child_id);
	static void detach(size_t id);
	static void listen(const sf::Event& event);
	static void enable(size_t id);
	static void disable(size_t id);

	template <typename  Object, typename Type>
	static void connect(size_t id, Object* object, void(Type::*slot)());
	template <typename Object, typename Type>
	static void connect(size_t id, Object* object, void(Type::*slot)(size_t));
	template <typename Object, typename Type>
	static void connect(size_t id, Object* object, void(Type::*slot)(float, float));
	static void connect(size_t id, void(*slot)());
	static void connect(size_t id, void(*slot)(size_t));
	static void connect(size_t id, void(*slot)(float, float));

protected:

	friend class Event;

	static void attach_(size_t id);
	static void detach_(size_t id);

	static std::vector<Event*> m_attached_events;
	static std::map<size_t, Event*> m_events;
};

////////////////////////////////////////////////////////////
template <typename Type, typename... Args>
size_t EventManager::create(Args... args)
{
	static_assert(!std::is_base_of<Type, Event>::value, "EventManager does not support this type !");
	Type* type_ptr = new Type(args...);
	size_t id = type_ptr->id;
	m_events[id] = type_ptr;
	return id;
}

////////////////////////////////////////////////////////////
template <typename Object, typename Type>
void EventManager::connect(size_t id, Object* object, void(Type::*slot)())
{
	get(id).connect(object, slot);
}

////////////////////////////////////////////////////////////
template <typename Object, typename Type>
void EventManager::connect(size_t id, Object* object, void(Type::*slot)(size_t))
{
	get(id).connect(object, slot);
}

////////////////////////////////////////////////////////////
template <typename Object, typename Type>
void EventManager::connect(size_t id, Object* object, void(Type::*slot)(float, float))
{
	get(id).connect(object, slot);
}

}

#endif // ZGE_EVENT_MANAGER