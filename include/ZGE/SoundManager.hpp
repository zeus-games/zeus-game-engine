////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef SOUNDMANAGER_HPP
#define SOUNDMANAGER_HPP

////////////////////////////////////////////////////////////
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/Listener.hpp>
#include <map>

namespace ZGE
{

////////////////////////////////////////////////////////////
class SoundManager : public sf::Listener
{
public:

	SoundManager() = delete;
	SoundManager(const SoundManager& soundManager) = delete;
	static void clear();
	static bool erase(size_t id);
	static size_t createSound(size_t resource_id, bool play = false, bool loop = false, float volume = 100);
	static sf::Sound& getSound(size_t id);
	static void pause(bool enabled = true);
	static void playSound(size_t id);
	static void pauseSound(size_t id);
	static void stopSound(size_t id);

protected:

	static std::map<size_t, std::pair<sf::Sound, bool>> m_sounds;
};

}

#endif // SOUNDPLAYER_HPP