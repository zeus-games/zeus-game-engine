////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef ZGE_SIGNAL_HPP
#define ZGE_SIGNAL_HPP

#include <functional>
#include <vector>
#include <map>

namespace ZGE
{

class BaseSignal {};
class Reachable;

////////////////////////////////////////////////////////////
template<typename... Args>
class Signal : public BaseSignal
{
    typedef std::function<void(Args...)> Function;
    typedef std::function<void(Reachable*, Args...)> Slot;

public:

    Signal();
    Signal(const Signal<Args...>& signal) = delete;
    ~Signal();

    void connect(Function function);
    template<typename Object, typename Type, typename... TypeArgs>
    void connect(Object* object, void (Type::*slot)(TypeArgs...));
    void operator()(Args... arg);
    void call(Args... arg);
    void disconnect();
    void disconnect(Reachable* object);

protected:

    std::vector<Function> m_functions;
    std::map<Reachable*, std::vector<Slot>> m_slots;

    friend class Reachable;
    void _disconnect(Reachable* object);
};

}

#include <ZGE/Signal.inl>

#endif // ZGE_SIGNAL_HPP
