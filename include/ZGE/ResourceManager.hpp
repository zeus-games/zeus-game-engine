////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef RESOURCEMANAGER_HPP
#define RESOURCEMANAGER_HPP

////////////////////////////////////////////////////////////
#include <map>
#include <iostream>
#include <cassert>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

namespace ZGE
{

////////////////////////////////////////////////////////////
class ResourceManager
{
public:

    ResourceManager() = delete;
    ResourceManager(const ResourceManager& manager) = delete;

    template<typename ResourceType>
    static size_t create(const sf::String& filePath)
    {
        static_assert(check<ResourceType>(), "ResourceManager does not support this type !");
        ResourceType* ptr = new ResourceType();
        size_t type;
        if( std::is_same<ResourceType, sf::Texture>::value )
            type = Type::Texture;
        else if( std::is_same<ResourceType, sf::Texture>::value )
            type = Type::Font;
        else if( std::is_same<ResourceType, sf::SoundBuffer>::value )
            type = Type::SoundBuffer;
        bool open = ptr->loadFromFile(filePath);
        assert(open);
        m_resources[count] = {ptr, type};
        return count++;
    }

    static bool erase(size_t id);
    static bool clear();

    template<typename ResourceType>
    static ResourceType& get(size_t id)
    {
   		static_assert(check<ResourceType>(), "ResourceManager does not support this type !");
        auto it = m_resources.find(id);
        assert(it != m_resources.end());
        return *static_cast<ResourceType*>(it->second.first);
    }

protected:

    template<typename ResourceType>
    static constexpr bool check()
    {
        return std::is_same<ResourceType, sf::Texture>::value ||
               std::is_same<ResourceType, sf::Font>::value    ||
               std::is_same<ResourceType, sf::SoundBuffer>::value;
    }

   static std::map<size_t, std::pair<void*, size_t>> m_resources;
   static size_t count;
   enum Type { Texture, Font, SoundBuffer };
};

}

#endif // RESOURCEMANAGER_HPP