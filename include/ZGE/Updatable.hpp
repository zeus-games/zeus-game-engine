////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#ifndef ZGE_UPDATABLE
#define ZGE_UPDATABLE

#include <ZGE/TimerManager.hpp>
#include <ZGE/Reachable.hpp>

namespace ZGE
{

////////////////////////////////////////////////////////////
class Updatable : public Reachable
{
public:

	Updatable()
	{
		m_timer_update_id = TimerManager::create(sf::milliseconds(1), true, true, this, &Updatable::onUpdate);
	}

	virtual ~Updatable()
	{
		TimerManager::erase(m_timer_update_id);
	}

protected:

	friend class Timer;
	virtual void onUpdate() = 0;
	
	size_t m_timer_update_id;
};

}

#endif // ZGE_UPDATABLE