#include "swatter.hpp"
#include <iostream>
#include <cmath>
#include <ZGE/ConvertString.hpp>
#include <ZGE/TimerManager.hpp>

////////////////////////////////////////////////////////////
Swatter::Swatter(size_t texture_swatter_id, size_t angle_side, float pos_factor)
{
	// Création du sprite

	m_sprite_id = GraphicManager::createSprite(texture_swatter_id);

	// Configuration du sprite

	sf::Sprite& sprite = GraphicManager::get<sf::Sprite>(m_sprite_id);
	float width  = sprite.getLocalBounds().width,
		  height = sprite.getLocalBounds().height;
	sprite.setOrigin(width/2, height/2);
	float x = (1366-2*(width))*pos_factor + width/2;
	float y = (768-2*(height))*pos_factor + height/2;
	if( angle_side == 0 )
		sprite.move(-width/2, y);
	else if( angle_side == 90 )
		sprite.move(x, -width/2);
	else if( angle_side == 180 )
		sprite.move(1366 + width/2, y);
	else if( angle_side == 270 )
		sprite.move(x, 768 + width/2);
	sprite.rotate((float)angle_side);

	// Attachement du sprite au graphe principal

    GraphicManager::attach(m_sprite_id);

    // Création des timers

    m_timer1_id = TimerManager::create(sf::seconds(2),   true, false, this, &Swatter::onTime1);
    m_timer2_id = TimerManager::create(sf::seconds(2.1), true, false, this, &Swatter::onTime2);
    m_timer3_id = TimerManager::create(sf::seconds(3),   true, false, this, &Swatter::onTime3);

    std::cout << "swatter created!\n";
}

////////////////////////////////////////////////////////////
Swatter::~Swatter()
{
	GraphicManager::detach(m_sprite_id);
	GraphicManager::erase(m_sprite_id);
	TimerManager::erase(m_timer1_id);
    TimerManager::erase(m_timer2_id);
    TimerManager::erase(m_timer3_id);
}


////////////////////////////////////////////////////////////
bool Swatter::isCatched(const sf::Vector2f& p1, const sf::Vector2f& p2, const sf::Vector2f& p3, const sf::Vector2f& p4)
{
	sf::Sprite& sprite = GraphicManager::get<sf::Sprite>(m_sprite_id);
	sf::FloatRect rect = sprite.getGlobalBounds();
	return rect.contains(p1) || rect.contains(p2) || rect.contains(p3) || rect.contains(p4);
}

/*////////////////////////////////////////////////////////////
void Swatter::notify(const Message& message)
{
	if( message.header == "timer" )
    {
        if( message.param == m_timer1_id )
        {
        	TimerManager::erase(m_timer1_id);
        	m_speed = 6000;
			broadcast({"swatter", "catch", 0, this});
        }

        else if( message.param == m_timer2_id )
        {
        	TimerManager::erase(m_timer2_id);
        	m_speed = 0;
        }

        else if( message.param == m_timer3_id )
        {
        	TimerManager::erase(m_timer3_id);
        	broadcast({"delete", "", 0, this});
        }
    }
}*/

////////////////////////////////////////////////////////////
void Swatter::onUpdate()
{
	sf::Sprite& sprite = GraphicManager::get<sf::Sprite>(m_sprite_id);
	float timeStep = 0.001;
	float dist = m_speed * timeStep;
	float radians = 3.14159265 * sprite.getRotation() / 180;
	sprite.move(dist * std::cos(radians), dist * std::sin(radians));
}