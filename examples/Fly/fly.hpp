#ifndef FLY_HPP
#define FLY_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include <sstream>
#include <ZGE/ResourceManager.hpp>
#include <ZGE/EventManager.hpp>
#include <ZGE/GameObject.hpp>

using namespace ZGE;

////////////////////////////////////////////////////////////
class Fly : public GameObject
{
public:
	
	Fly(size_t texture_fly_id, size_t texture_bloody_id, size_t texture_stay_id, size_t soundbuffer_bzz_id);
	~Fly();

protected:

	friend class Event;

	void onUpdate();
	void go();
	void turnLeft();
	void turnRight();
	void stopTurn();
	void onWallCollision();

	int m_orientation = 0;
	bool m_stay = false;
	float m_speed = 200, m_angleSpeed = 200, m_stayAngle = 0;
	sf::FloatRect m_sandbox = {50, 50, 1366 - 100, 768 - 100};

	size_t m_sprite_id, m_sound_bzz_id;
	size_t m_texture_fly_id, m_texture_bloody_id, m_texture_stay_id, m_soundbuffer_bzz_id;
	size_t m_event_left_key_released_id, m_event_right_key_released_id, m_event_up_key_released_id;
};

#endif // FLY_HPP