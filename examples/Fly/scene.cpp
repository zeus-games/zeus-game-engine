#include "Scene.hpp"
#include "Fly.hpp"
#include "Swatter.hpp"
#include <ZGE/GameObjectManager.hpp>
#include <ZGE/ResourceManager.hpp>
#include <ZGE/SoundManager.hpp>
#include <ZGE/TimerManager.hpp>

////////////////////////////////////////////////////////////
Scene::Scene()
{
    // Chargement des textures
    std::cout << "Textures loading... ";
    m_texture_outdoor_id = ResourceManager::create<sf::Texture>("media/outdoor.png");
    m_texture_pot_id     = ResourceManager::create<sf::Texture>("media/pot.png");
    m_texture_heart_id   = ResourceManager::create<sf::Texture>("media/heart.png");
    m_texture_fly_id     = ResourceManager::create<sf::Texture>("media/fly.png");
    m_texture_bloody_id  = ResourceManager::create<sf::Texture>("media/bloody.png");
    m_texture_stay_id    = ResourceManager::create<sf::Texture>("media/stay.png");
    m_texture_swatter_id = ResourceManager::create<sf::Texture>("media/swatter.png");
    std::cout << "OK\n";

    // Chargement des sons
    std::cout << "Sounds loading... ";
    m_soundbuffer_bzz_id = ResourceManager::create<sf::SoundBuffer>("media/bzz.ogg");
    std::cout << "OK\n";

    // Création des sprites
    m_sprite_pot_id     = GraphicManager::createSprite(m_texture_pot_id, {100, 480});
    m_sprite_outdoor_id = GraphicManager::createSprite(m_texture_outdoor_id);
    m_sprite_heart_id   = GraphicManager::createSprite(m_texture_heart_id);

    // Attachement au graphe de scene principal
    GraphicManager::attach(m_sprite_outdoor_id);
    GraphicManager::attach(m_sprite_pot_id);
    GraphicManager::attach(m_sprite_heart_id);

    // Création des timers
    m_timer_hazard_id = TimerManager::create(sf::seconds(5), true, true, this, &Scene::onHazard);

    // Création de la mouche
    createFly();
}

////////////////////////////////////////////////////////////
void Scene::createFly()
{
    m_fly_id = GameObjectManager::create<Fly>(m_texture_fly_id, m_texture_bloody_id, m_texture_stay_id, m_soundbuffer_bzz_id);
}

////////////////////////////////////////////////////////////
void Scene::createSwatter()
{
    std::uniform_int_distribution<int> i1(0, 99);
    std::uniform_int_distribution<int> i2(0, 99);

    size_t side_angle = static_cast<size_t>(i1(m_engine)/33)*90;
    float pos_factor  = static_cast<float> (i2(m_engine))/100;

    m_swatter_id = GameObjectManager::create<Swatter>(m_texture_swatter_id, side_angle, pos_factor);
}

////////////////////////////////////////////////////////////
void Scene::pause()
{
    static bool pause = false;
    pause = !pause;

    TimerManager::pause(pause);
	SoundManager::pause(pause);
}

////////////////////////////////////////////////////////////
void Scene::onHazard()
{
	createSwatter();
}