#ifndef SCENE_HPP
#define SCENE_HPP

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <map>
#include <random>
#include <ZGE/GameObject.hpp>

using namespace ZGE;

////////////////////////////////////////////////////////////
class Scene : public Reachable
{
public:

	Scene();
	~Scene() {}
	void pause();

protected:

	void createFly();
	void createSwatter();
	void onHazard();

	std::mt19937 m_engine;

	size_t m_texture_outdoor_id,
		   m_texture_pot_id,
		   m_texture_heart_id,
		   m_texture_fly_id,
		   m_texture_bloody_id,
		   m_texture_stay_id,
		   m_texture_swatter_id,
  		   m_soundbuffer_bzz_id,
		   m_timer_update_id,
		   m_timer_hazard_id,
		   m_sprite_pot_id,
		   m_sprite_outdoor_id,
		   m_sprite_heart_id,
		   m_fly_id,
		   m_swatter_id;
};

#endif // SCENE_HPP