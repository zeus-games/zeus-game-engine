#include "fly.hpp"
#include <ZGE/GraphicManager.hpp>
#include <ZGE/SoundManager.hpp>
#include "swatter.hpp"

////////////////////////////////////////////////////////////
Fly::Fly(size_t texture_fly_id, size_t texture_bloody_id, size_t texture_stay_id, size_t soundbuffer_bzz_id) :
m_texture_fly_id(texture_fly_id),
m_texture_bloody_id(texture_bloody_id),
m_texture_stay_id(texture_stay_id),
m_soundbuffer_bzz_id(soundbuffer_bzz_id),
m_sprite_id(GraphicManager::createSprite(texture_fly_id, {200, 200})),
m_sound_bzz_id(SoundManager::createSound(soundbuffer_bzz_id, true, true)),
m_event_left_key_released_id(EventManager::create<KeyReleased>(sf::Keyboard::Left)),
m_event_right_key_released_id(EventManager::create<KeyReleased>(sf::Keyboard::Right)),
m_event_up_key_released_id(EventManager::create<KeyPressed>(sf::Keyboard::Up))
{
	// Configuration du sprite
	GraphicManager::center(m_sprite_id);
	GraphicManager::attach(m_sprite_id);

	// Configuration des contrôles
	EventManager::connect(m_event_left_key_released_id, this, &Fly::stopTurn);
	EventManager::connect(m_event_right_key_released_id, this, &Fly::stopTurn);
	EventManager::connect(m_event_up_key_released_id, this, &Fly::go);
	EventManager::attach(m_event_left_key_released_id, true);
	EventManager::attach(m_event_right_key_released_id, true);
	EventManager::attach(m_event_up_key_released_id, true);
}

////////////////////////////////////////////////////////////
Fly::~Fly()
{
	GraphicManager::erase(m_sprite_id);
	SoundManager::erase(m_sound_bzz_id);
	EventManager::erase(m_event_up_key_released_id);
	EventManager::erase(m_event_left_key_released_id);
	EventManager::erase(m_event_right_key_released_id);
}

////////////////////////////////////////////////////////////
void Fly::go()
{
	if( m_stay )
	{
		SoundManager::playSound(m_sound_bzz_id);

		GraphicManager::setScale(m_sprite_id, 1, 1);
		GraphicManager::setTexture(m_sprite_id, m_texture_fly_id);
		GraphicManager::center(m_sprite_id);
		GraphicManager::setRotation(m_sprite_id, m_stayAngle - 90);
		
		m_stay = false;
	}
}

////////////////////////////////////////////////////////////
void Fly::turnLeft()
{
	if( !m_stay && m_orientation != -1 )
	{
		SoundManager::getSound(m_sound_bzz_id).setPitch(1.15);
		m_orientation = -1;
	}
}

////////////////////////////////////////////////////////////
void Fly::turnRight()
{
	if( !m_stay && m_orientation != 1 )
	{
		SoundManager::getSound(m_sound_bzz_id).setPitch(1.15);
		m_orientation = 1;
	}
}

////////////////////////////////////////////////////////////
void Fly::stopTurn()
{
	if( !m_stay && m_orientation != 0 )
	{
		SoundManager::getSound(m_sound_bzz_id).setPitch(1);
		m_orientation = 0;
	}
}

////////////////////////////////////////////////////////////
void Fly::onWallCollision()
{
	// Ne plus tourner
	stopTurn();

	// Arrêt du son
	SoundManager::stopSound(m_sound_bzz_id);

	// Mise à jour du sprite
	GraphicManager::setTexture(m_sprite_id, m_texture_stay_id);
	GraphicManager::center(m_sprite_id);

	// Obtention de la position
	sf::Vector2f pos = GraphicManager::getPosition(m_sprite_id);

	// Détection du mur
	if( pos.x <= m_sandbox.left )
		m_stayAngle = 90;
	else if( pos.y <= m_sandbox.top )
		m_stayAngle = 180;
	else if( pos.x >= m_sandbox.left + m_sandbox.width )
		m_stayAngle = -90;
	else if( pos.y >= m_sandbox.top + m_sandbox.height )
		m_stayAngle = 0;
	
	// Ajustement de l'angle
	GraphicManager::setRotation(m_sprite_id, m_stayAngle);

	// Détermination du nouvel angle
	float angle = GraphicManager::getRotation(m_sprite_id);

	// Ajustement du sens
	if( angle > m_stayAngle + 90 && angle < m_stayAngle + 180 )
		GraphicManager::setScale(m_sprite_id, -1, 1);

	// Mouche figée
	m_stay = true;
}

////////////////////////////////////////////////////////////
void Fly::onUpdate()
{
	float timeStep = 0.001;

	if( !m_stay )
	{
		if( sf::Keyboard::isKeyPressed(sf::Keyboard::Left ) )
			turnLeft();

		else if( sf::Keyboard::isKeyPressed(sf::Keyboard::Right) )
			turnRight();

		// Mise à jour de la position et de l'angle
		GraphicManager::rotate(m_sprite_id, m_orientation * m_angleSpeed * timeStep);
		float dist = m_speed * timeStep, radians = 3.14159265 * GraphicManager::getRotation(m_sprite_id) / 180;
		GraphicManager::move(m_sprite_id, dist * std::cos(radians), dist * std::sin(radians));
		
		// Test de collision aux murs
		if( !m_sandbox.contains(GraphicManager::getPosition(m_sprite_id)) )
			onWallCollision();
	}
}