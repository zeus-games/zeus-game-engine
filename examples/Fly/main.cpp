#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <ZGE/ResourceManager.hpp>
#include <ZGE/GraphicManager.hpp>
#include <ZGE/SoundManager.hpp>
#include <ZGE/TimerManager.hpp>
#include <ZGE/EventManager.hpp>
#include <ZGE/GameObjectManager.hpp>
#include <ZGE/Updatable.hpp>
#include "scene.hpp"

using namespace ZGE;

////////////////////////////////////////////////////////////
int main()
{
    sf::RenderWindow window(sf::VideoMode(1366, 768, 32), "Fly", sf::Style::None);
    window.setVerticalSyncEnabled(40);
    window.setKeyRepeatEnabled(false);

    Scene scene;

    while( window.isOpen() )
    {
        sf::sleep(sf::microseconds(100));

        sf::Event event;
      
        while( window.pollEvent(event) )
        {     
            if( event.type == sf::Event::Closed )
                window.close();
         
            else if( event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape )
                window.close();

            else EventManager::listen(event);
        }

        TimerManager::update();

        window.clear();
        GraphicManager::render(window);
        window.display();
    }

    ResourceManager::clear();
    SoundManager::clear();
    GraphicManager::clear();
    TimerManager::clear();
    GameObjectManager::clear();
    EventManager::clear();
   
    return EXIT_SUCCESS;
}