#ifndef SWATTER_HPP
#define SWATTER_HPP

#include <SFML/System.hpp>
#include <ZGE/GraphicManager.hpp>
#include <ZGE/GameObject.hpp>

using namespace ZGE;

////////////////////////////////////////////////////////////
class Swatter : public GameObject
{
public:

	Swatter(size_t texture_swatter_id, size_t angle_side, float pos_factor);
	~Swatter();
	bool isCatched(const sf::Vector2f& p1, const sf::Vector2f& p2, const sf::Vector2f& p3, const sf::Vector2f& p4);
	void onUpdate();

private:

	
	void onTime1() {}
	void onTime2() {}
	void onTime3() {}

	float m_speed = 0;
	size_t m_sprite_id,
		   m_timer1_id,
		   m_timer2_id,
		   m_timer3_id;
};

#endif // SWATTER_HPP