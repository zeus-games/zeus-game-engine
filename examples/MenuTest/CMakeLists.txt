add_executable( 
  MenuTest
  main.cpp
)

target_link_libraries(
  MenuTest
  ZGE
  ${SFML_GRAPHICS_LIBRARY}
  ${SFML_WINDOW_LIBRARY}
  ${SFML_AUDIO_LIBRARY}
  ${SFML_SYSTEM_LIBRARY}
)