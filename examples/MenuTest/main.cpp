#include <ZGE/WidgetManager.hpp>
#include <ZGE/TimerManager.hpp>
#include <ZGE/GraphicManager.hpp>
#include <ZGE/EventManager.hpp>
#include <initializer_list>

using namespace ZGE;

////////////////////////////////////////////////////////////
int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600, 32), "MenuTest");
    window.setVerticalSyncEnabled(40);
    window.setKeyRepeatEnabled(false);

    GraphicManager::setDefaultFontPath("media/arial.ttf");

    size_t nickname, difficulty, resolution, windowed, back, options_menu, play, options, highscores, credits, quit, main_menu;

    nickname   = WidgetManager::create<MenuItemEdition>("Nickname")
                .connect([&] () { window.close(); });
    difficulty = WidgetManager::create<MenuItemSelection>("Difficulty", "Easy/Normal/Hard/Hardest");
    resolution = WidgetManager::create<MenuItemSelection>("Resolution", "800x600/1024x768");
    windowed   = WidgetManager::create<MenuItemSelection>("Display", "Fullscreen/Windowed");
    back       = WidgetManager::create<MenuItemValidation>("Back")
                .connect(std::bind(WidgetManager::attach, std::ref(main_menu)))
                .connect(std::bind(WidgetManager::detach, std::ref(options_menu)));
    play       = WidgetManager::create<MenuItemValidation>("Play");
    options    = WidgetManager::create<MenuItemValidation>("Options")
                .connect(std::bind(WidgetManager::attach, std::ref(options_menu)))
                .connect(std::bind(WidgetManager::detach, std::ref(main_menu)));
    highscores = WidgetManager::create<MenuItemValidation>("Highscores");
    credits    = WidgetManager::create<MenuItemValidation>("Credits");
    quit       = WidgetManager::create<MenuItemValidation>("Quit")
                .connect([&] () { window.close(); });
    main_menu  = WidgetManager::create<Menu>("Main", Alignment::Left)
                .attach(play)
                .attach(options)
                .attach(highscores)
                .attach(credits)
                .attach(quit)
                .align({0, 0, 800, 600}, Alignment::MiddleMiddle);
    options_menu = WidgetManager::create<Menu>("Options", Alignment::Left)
                .attach(nickname)
                .attach(difficulty)
                .attach(resolution)
                .attach(windowed)
                .attach(back)
                .align({0, 0, 800, 600}, Alignment::MiddleMiddle);

    WidgetManager::attach(main_menu);

    while( window.isOpen() )
    {
        sf::sleep(sf::microseconds(100));

        sf::Event event;
      
        while( window.pollEvent(event) )
        {     
            if( event.type == sf::Event::Closed )
                window.close();
         
            else if( event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape )
                window.close();

            else EventManager::listen(event);
        }

        TimerManager::update();

        window.clear();
        GraphicManager::render(window);
        window.display();
    }

    WidgetManager::clear();
    GraphicManager::clear();
    EventManager::clear();
    TimerManager::clear();
   
    return EXIT_SUCCESS;
}