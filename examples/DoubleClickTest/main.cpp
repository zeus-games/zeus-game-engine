#include <SFML/Graphics.hpp>
#include <ZGE/TimerManager.hpp>
#include <ZGE/EventManager.hpp>

void p1()
{
    std::cout << "p1" << std::endl;
}

void r1()
{
    std::cout << "r1" << std::endl;
}

void p2()
{
    std::cout << "p2" << std::endl;
}

void r2()
{
    std::cout << "r2" << std::endl;
}

void to()
{
    std::cout << "to" << std::endl;
}

using namespace ZGE;

////////////////////////////////////////////////////////////
int main()
{
    sf::RenderWindow window(sf::VideoMode(200, 200, 32), "DoubleClickTest");
    window.setVerticalSyncEnabled(40);
    window.setKeyRepeatEnabled(false);

    size_t p1_event = EventManager::create<MouseButtonPressed>(sf::Mouse::Left);
    size_t r1_event = EventManager::create<MouseButtonReleased>(sf::Mouse::Left);
    size_t p2_event = EventManager::create<MouseButtonPressed>(sf::Mouse::Left);
    size_t r2_event = EventManager::create<MouseButtonReleased>(sf::Mouse::Left);
    size_t to_event = EventManager::create<TimeOut>(sf::seconds(0.5));

    EventManager::attach(p1_event, r1_event);
    EventManager::attach(p1_event, to_event); 
    EventManager::attach(r1_event, p2_event);
    EventManager::attach(r1_event, to_event);
    EventManager::attach(p2_event, r2_event);
    EventManager::attach(p2_event, to_event);
    EventManager::attach(r2_event, p1_event);
    EventManager::attach(to_event, p1_event);

    EventManager::connect(p1_event, p1);
    EventManager::connect(r1_event, r1);
    EventManager::connect(p2_event, p2);
    EventManager::connect(r2_event, r2);
    EventManager::connect(to_event, to);

    EventManager::attach(p1_event);

    while( window.isOpen() )
    {
        sf::sleep(sf::microseconds(100));

        sf::Event event;
      
        while( window.pollEvent(event) )
        {     
            if( event.type == sf::Event::Closed )
                window.close();
         
            else if( event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape )
                window.close();

            else EventManager::listen(event);
        }

        TimerManager::update();

        window.clear();
        window.display();
    }

    EventManager::clear();
   
    return EXIT_SUCCESS;
}