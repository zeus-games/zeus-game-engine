////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#include <ZGE/ResourceManager.hpp>

namespace ZGE
{

////////////////////////////////////////////////////////////
std::map<size_t, std::pair<void*, size_t>> ResourceManager::m_resources;
size_t ResourceManager::count = 0;

////////////////////////////////////////////////////////////
bool ResourceManager::erase(size_t id)
{
    auto it = m_resources.find(id);
    if( it == m_resources.end() )
        return false;
    size_t type =  it->second.second;
    if( type == Type::Texture )
    	delete static_cast<sf::Texture*>(it->second.first);
    else if( type == Type::Font )
    	delete static_cast<sf::Font*>(it->second.first);
    else if( type == Type::SoundBuffer )
    	delete static_cast<sf::SoundBuffer*>(it->second.first);
    m_resources.erase(it);
    return true;
}

////////////////////////////////////////////////////////////
bool ResourceManager::clear()
{
    for( auto it = m_resources.begin(); it != m_resources.end(); ++it )
    {
    	size_t type =  it->second.second;
	    if( type == Type::Texture )
	    	delete static_cast<sf::Texture*>(it->second.first);
	    else if( type == Type::Font )
	    	delete static_cast<sf::Font*>(it->second.first);
	    else if( type == Type::SoundBuffer )
	    	delete static_cast<sf::SoundBuffer*>(it->second.first);
	}
}

}