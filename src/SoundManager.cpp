////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#include "ZGE/SoundManager.hpp"
#include "ZGE/ResourceManager.hpp"
#include <SFML/Audio/SoundSource.hpp>

namespace ZGE
{

std::map<size_t, std::pair<sf::Sound, bool>> SoundManager::m_sounds;

////////////////////////////////////////////////////////////
void SoundManager::clear()
{
	m_sounds.clear();
}

////////////////////////////////////////////////////////////
bool SoundManager::erase(size_t id)
{
    auto it = m_sounds.find(id);
    if( it == m_sounds.end() )
        return false;
    m_sounds.erase(it);
    return true;
}

////////////////////////////////////////////////////////////
size_t SoundManager::createSound(size_t resource_id, bool play, bool loop, float volume)
{
	static size_t count;
	sf::Sound sound;
	sf::SoundBuffer* buff_ = &ResourceManager::get<sf::SoundBuffer>(resource_id);
	sound.setBuffer(*buff_);
	sound.setLoop(loop);
	sound.setVolume(volume);
	m_sounds[count] = std::pair<sf::Sound, bool>(sound, false);
	if( play )
		m_sounds[count].first.play();
	return count++;
}

////////////////////////////////////////////////////////////
sf::Sound& SoundManager::getSound(size_t resource_id)
{
	auto it = m_sounds.find(resource_id);
    assert(it != m_sounds.end());
    return it->second.first;
}

////////////////////////////////////////////////////////////
void SoundManager::pause(bool enabled)
{
	if( enabled )
	{
		for( auto& pair : m_sounds )
		{
			pair.second.second = pair.second.first.getStatus() == sf::SoundSource::Status::Playing;
			pair.second.first.pause();
		}
	}

	else
	{
		for( auto& pair : m_sounds )
		{
			if( pair.second.second == sf::SoundSource::Status::Playing )
				pair.second.first.play();
		}
	}
}

////////////////////////////////////////////////////////////
void SoundManager::playSound(size_t id)
{
	getSound(id).play();
}

////////////////////////////////////////////////////////////
void SoundManager::pauseSound(size_t id)
{
	getSound(id).pause();
}

////////////////////////////////////////////////////////////
void SoundManager::stopSound(size_t id)
{
	getSound(id).stop();
}

}