////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#include "ZGE/EventManager.hpp"
#include "ZGE/TimerManager.hpp"
#include <cassert>

namespace ZGE
{

////////////////////////////////////////////////////////////
Event::Event() {}

////////////////////////////////////////////////////////////
Event::~Event()
{
	for( auto& parent : m_parents )
		parent->detach(*this);
}

////////////////////////////////////////////////////////////
void Event::trigger()
{
	m_triggered = true;

	disable();

	m_signal();
	m_signal_uint(m_uint);
	m_signal_2f(m_2f[0], m_2f[1]);

	for( auto& parent : m_parents )
		EventManager::detach_(parent->id);

	if( !m_parents.empty() )
		EventManager::attach_(id);

	if( m_children.empty() )
	{
		if( repeated )
			m_triggered = false;
		
		else EventManager::detach_(id);
	}

	for( auto& child : m_children )
	{
		child->m_triggered = false;
		child->enable();
	}
}

////////////////////////////////////////////////////////////
void Event::listen(const sf::Event& event)
{
	if( m_triggered )
		for( auto& child : m_children )
			child->listen(event);

	else if( find(event) )
		trigger();
}

////////////////////////////////////////////////////////////
void Event::attach(Event& child)
{
	m_children.push_back(&child);
	child.m_parents.push_back(this);
}

////////////////////////////////////////////////////////////
void Event::detach(Event& child)
{
	for( auto it = m_children.begin(); it != m_children.end(); ++it )
		if( *it == &child )
		{
			for( auto ik = child.m_parents.begin(); ik != child.m_parents.end(); ++ik )
				if( *ik == this )
				{
					child.m_parents.erase(ik);
					break;
				}

			m_children.erase(it);
			break;
		}
}

////////////////////////////////////////////////////////////
void Event::enable()
{
	m_enabled = true;
}

////////////////////////////////////////////////////////////
void Event::disable()
{
	m_enabled = false;
}

////////////////////////////////////////////////////////////
void Event::connect(void(*slot)())
{
	m_signal.connect(slot);
}

////////////////////////////////////////////////////////////
void Event::connect(void(*slot)(size_t))
{
	m_signal_uint.connect(slot);
}

////////////////////////////////////////////////////////////
void Event::connect(void(*slot)(float, float))
{
	m_signal_2f.connect(slot);
}

////////////////////////////////////////////////////////////
TimeOut::TimeOut(sf::Time time) { m_timer_id = TimerManager::create(time, false, false, this, &Event::trigger); }
TimeOut::~TimeOut() { TimerManager::erase(m_timer_id); }
void TimeOut::enable() { TimerManager::startTimer(m_timer_id); }
void TimeOut::disable() { TimerManager::stopTimer(m_timer_id); }
bool TimeOut::find(const sf::Event& sf_event) { return false; }

////////////////////////////////////////////////////////////
KeyPressed::KeyPressed() : m_check(false) {}
KeyPressed::KeyPressed(size_t key) : m_key(key), m_check(true) {}
bool KeyPressed::find(const sf::Event& event) { if( event.type == sf::Event::KeyPressed ) { m_uint = event.key.code; if( m_check ) { if( event.key.code == m_key ) return true; } else return true; } return false; }

////////////////////////////////////////////////////////////
KeyReleased::KeyReleased() : m_check(false) {}
KeyReleased::KeyReleased(size_t key) : m_key(key), m_check(true) {}
bool KeyReleased::find(const sf::Event& event)  { if( event.type == sf::Event::KeyReleased ) { m_uint = event.key.code; if( m_check ) { if( event.key.code == m_key ) return true; } else return true; } return false; }

////////////////////////////////////////////////////////////
TextEntered::TextEntered() {}
bool TextEntered::find(const sf::Event& event) { m_uint = event.text.unicode; return event.type == sf::Event::TextEntered; }

////////////////////////////////////////////////////////////
MouseMoved::MouseMoved() {}
bool MouseMoved::find(const sf::Event& event) { m_2f[1] = event.mouseMove.x; m_2f[0] = event.mouseMove.y; return event.type == sf::Event::Event::MouseMoved; }

////////////////////////////////////////////////////////////
MouseButtonPressed::MouseButtonPressed(size_t button) : m_button(button) {}
bool MouseButtonPressed::find(const sf::Event& event) { m_2f[1] = event.mouseButton.x; m_2f[0] = event.mouseButton.y; return event.type == sf::Event::Event::MouseButtonPressed && event.mouseButton.button == m_button; }

////////////////////////////////////////////////////////////
MouseButtonReleased::MouseButtonReleased(size_t button) : m_button(button) {}
bool MouseButtonReleased::find(const sf::Event& event) { m_2f[1] = event.mouseButton.x; m_2f[0] = event.mouseButton.y; return event.type == sf::Event::Event::MouseButtonReleased && event.mouseButton.button == m_button; }

////////////////////////////////////////////////////////////
std::vector<Event*> EventManager::m_attached_events;
std::map<size_t, Event*> EventManager::m_events;

////////////////////////////////////////////////////////////
bool EventManager::erase(size_t id)
{
	auto it = m_events.find(id);

	if( it != m_events.end() )
	{
		detach(id);
		delete it->second;
		m_events.erase(it);
	}
}

////////////////////////////////////////////////////////////
Event& EventManager::get(size_t id)
{
	auto it = m_events.find(id);
	assert(it != m_events.end());
	return *it->second;
}

////////////////////////////////////////////////////////////
void EventManager::attach_(size_t id)
{
	Event& event = get(id);

	for( auto it = m_attached_events.begin(); it != m_attached_events.end(); ++it )
		if( *it == &event )
		{
			return;
		}

	m_attached_events.push_back(&event);
}

////////////////////////////////////////////////////////////
void EventManager::attach(size_t id, bool repeated)
{
	Event& event = get(id);

	for( auto it = m_attached_events.begin(); it != m_attached_events.end(); ++it )
		if( *it == &event )
		{
			return;
		}

	event.enable();
	event.repeated = repeated;
	m_attached_events.push_back(&event);
}

////////////////////////////////////////////////////////////
void EventManager::attach(size_t parent_id, size_t child_id)
{
	get(parent_id).attach(get(child_id));
}

////////////////////////////////////////////////////////////
void EventManager::detach_(size_t id)
{
	Event& event = get(id);

	if( event.repeated )
		return;

	for( auto it = m_attached_events.begin(); it != m_attached_events.end(); ++it )
		if( *it == &event )
		{
			event.m_triggered = false;
			m_attached_events.erase(it);
			break;
		}
}

////////////////////////////////////////////////////////////
void EventManager::detach(size_t id)
{
	Event& event = get(id);

	for( auto it = m_attached_events.begin(); it != m_attached_events.end(); ++it )
		if( *it == &event )
		{
			event.m_triggered = false;
			m_attached_events.erase(it);
			break;
		}
}

////////////////////////////////////////////////////////////
void EventManager::enable(size_t id)
{
	get(id).enable();
}

////////////////////////////////////////////////////////////
void EventManager::disable(size_t id)
{
	get(id).disable();
}

////////////////////////////////////////////////////////////
void EventManager::clear()
{
	for( auto& pair : m_events )
		delete pair.second;
}

////////////////////////////////////////////////////////////
void EventManager::listen(const sf::Event& sf_event)
{
    for( auto& event : m_attached_events )
		event->listen(sf_event);
}

////////////////////////////////////////////////////////////
void EventManager::connect(size_t id, void(*slot)())
{
	get(id).connect(slot);
}

////////////////////////////////////////////////////////////
void EventManager::connect(size_t id, void(*slot)(size_t))
{
	get(id).connect(slot);
}

////////////////////////////////////////////////////////////
void EventManager::connect(size_t id, void(*slot)(float, float))
{
	get(id).connect(slot);
}

}