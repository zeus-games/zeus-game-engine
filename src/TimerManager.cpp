////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#include "ZGE/TimerManager.hpp"
#include <iostream>
#include <cassert>

namespace ZGE
{

////////////////////////////////////////////////////////////
Timer::Timer(const sf::Time& time, bool started, bool repeated) :
m_started(started),
m_repeated(repeated),
m_timeToReach(time)
{
	if( m_timeToReach < sf::milliseconds(1) )
		m_timeToReach = sf::milliseconds(1);
}

////////////////////////////////////////////////////////////
Timer::Timer(const sf::Time& time, bool started, bool repeated, std::function<void()> function) :
Timer(time, repeated, started)
{
	signal.connect(function);
}

////////////////////////////////////////////////////////////
void Timer::start()
{
	m_started = true;
}

////////////////////////////////////////////////////////////
void Timer::pause()
{
	m_started = false;
}

////////////////////////////////////////////////////////////
void Timer::restart()
{
	m_started = true;
	m_currentTime = sf::Time::Zero;
}

////////////////////////////////////////////////////////////
void Timer::stop()
{
	m_started = false;
	m_currentTime = sf::Time::Zero;
}

////////////////////////////////////////////////////////////
void Timer::update()
{
	if( m_started )
	{
		m_currentTime+=sf::milliseconds(1);

		if( m_currentTime == m_timeToReach )
		{
			if( m_repeated )
				m_currentTime = sf::Time::Zero;

			else m_started = false;

			signal();
		}
	}
}

////////////////////////////////////////////////////////////
std::map<size_t, Timer*> TimerManager::m_timers;
bool TimerManager::m_pause = false;
sf::Clock TimerManager::m_clock;

////////////////////////////////////////////////////////////
void TimerManager::clear()
{
	for( auto it = m_timers.begin(); it != m_timers.end(); ++it )
		delete it->second;
}

////////////////////////////////////////////////////////////
size_t TimerManager::create(const sf::Time& time, bool started, bool repeated)
{
	static size_t count;
	m_timers[count] = new Timer(time, started, repeated);
	return count++;
}

////////////////////////////////////////////////////////////
size_t TimerManager::create(const sf::Time& time, bool started, bool repeated, std::function<void()> function)
{
	static size_t count;
	m_timers[count] = new Timer(time, started, repeated, function);
	return count++;
}

////////////////////////////////////////////////////////////
Timer& TimerManager::get(size_t id)
{
	auto it = m_timers.find(id);
    assert(it != m_timers.end());
    return *it->second;
}

////////////////////////////////////////////////////////////
bool TimerManager::erase(size_t id)
{
    auto it = m_timers.find(id);
    if( it == m_timers.end() )
        return false;
    m_timers.erase(it);
    return true;
}

////////////////////////////////////////////////////////////
void TimerManager::pause(bool enabled)
{
	m_pause = enabled;
	if( m_pause )
		m_clock.restart();
}

////////////////////////////////////////////////////////////
void TimerManager::startTimer(size_t id)
{
	get(id).start();
}

////////////////////////////////////////////////////////////
void TimerManager::pauseTimer(size_t id)
{
	get(id).pause();
}

////////////////////////////////////////////////////////////
void TimerManager::restartTimer(size_t id)
{
	get(id).restart();
}

////////////////////////////////////////////////////////////
void TimerManager::stopTimer(size_t id)
{
	get(id).stop();
}

////////////////////////////////////////////////////////////
void TimerManager::update()
{
	static sf::Time timer;

	if( !m_pause )
	{
		timer+=m_clock.restart();

		while( timer >= sf::milliseconds(1) )
		{
			timer-=sf::milliseconds(1);

			for( auto& pair : m_timers )
				pair.second->update();
		}
	}
}

}