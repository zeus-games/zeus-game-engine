////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#include <ZGE/GraphicManager.hpp>
#include <ZGE/ResourceManager.hpp>
#include <iostream>

namespace ZGE
{

////////////////////////////////////////////////////////////
Graph::Graph(bool show) :
showed(show)
{}

////////////////////////////////////////////////////////////
Graph& Graph::attach(size_t id)
{
	sf::Drawable* drawable_ = &GraphicManager::get<sf::Drawable>(id);
	auto it = find(drawable_);

	if( it == m_drawables.end() )
		m_drawables.push_back(drawable_);

	else
	{
		std::cerr << "Error : Drawable \"" << id << "\" already attached to the graph !";
		//std::getwchar();
		//exit(EXIT_FAILURE);
	}

	return *this;
}

////////////////////////////////////////////////////////////
Graph& Graph::detach(size_t id)
{
	sf::Drawable* drawable_ = &GraphicManager::get<sf::Drawable>(id);
	m_drawables.erase(find(drawable_));

	return *this;
}

////////////////////////////////////////////////////////////
Graph& Graph::setDepth(size_t id, size_t range)
{
	if( range < m_drawables.size() )
	{
		sf::Drawable* drawable_ = &GraphicManager::get<sf::Drawable>(id);
		auto it = find(drawable_);

		if( it != m_drawables.end() )
		{
			detach(id);
			m_drawables.insert(m_drawables.begin() + range, drawable_);
		}
	}

	else
	{
		std::cerr << "Error : Range too high in the graph !";
		std::getwchar();
		exit(-1);
	}

	return *this;
}

////////////////////////////////////////////////////////////
std::vector<sf::Drawable*>::iterator Graph::find(sf::Drawable* drawable_)
{
	auto it = m_drawables.begin();

	for( ; it != m_drawables.end(); it++ )
		if( *it == drawable_ )
			return it;
	
	return it++;
}

////////////////////////////////////////////////////////////
void Graph::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if( showed )
	{
		states.transform.combine(getTransform());

		for( auto& drawable : m_drawables )
			target.draw(*drawable, states);
	}
}

////////////////////////////////////////////////////////////
Graph GraphicManager::m_graph;
std::map<size_t, sf::Drawable*> GraphicManager::m_drawables;
size_t GraphicManager::count = 0;
size_t GraphicManager::m_default_font_id = 0;

////////////////////////////////////////////////////////////
void GraphicManager::clear()
{
	for( auto& pair : m_drawables )
		delete pair.second;
}

////////////////////////////////////////////////////////////
bool GraphicManager::erase(size_t id)
{
	auto it = m_drawables.find(id);
    if( it == m_drawables.end() )
        return false;
    delete it->second;
    m_drawables.erase(it);
    return true;
}

////////////////////////////////////////////////////////////
void GraphicManager::render(sf::RenderTarget& target)
{
	target.draw(m_graph);
}

////////////////////////////////////////////////////////////
size_t GraphicManager::createSprite(size_t resource_id, const sf::Vector2f& pos)
{
	sf::Texture& texture = ResourceManager::get<sf::Texture>(resource_id);
	size_t id = create<sf::Sprite>();
	sf::Sprite& sprite = get<sf::Sprite>(id);
	sprite.setTexture(texture);
	sprite.setPosition(pos);

	return id;
}

////////////////////////////////////////////////////////////
size_t GraphicManager::createText(size_t resource_id)
{
	sf::Font& font = ResourceManager::get<sf::Font>(resource_id);
	size_t id = create<sf::Text>();
	get<sf::Text>(id).setFont(font);

	return id;
}

////////////////////////////////////////////////////////////
size_t GraphicManager::createText()
{
	sf::Font& font = ResourceManager::get<sf::Font>(m_default_font_id);
	size_t id = create<sf::Text>();
	get<sf::Text>(id).setFont(font);

	return id;
}

////////////////////////////////////////////////////////////
void GraphicManager::setDefaultFontPath(const sf::String& path_str)
{
	m_default_font_id = ResourceManager::create<sf::Font>(path_str);
}

////////////////////////////////////////////////////////////
void GraphicManager::attach(size_t id)
{
	m_graph.attach(id);
}

////////////////////////////////////////////////////////////
void GraphicManager::detach(size_t id)
{
	m_graph.detach(id);
}

////////////////////////////////////////////////////////////
void GraphicManager::rotate(size_t id, float angle)
{
	sf::Sprite& tr = get<sf::Sprite>(id);
	tr.rotate(angle);
}

////////////////////////////////////////////////////////////
void GraphicManager::move(size_t id, float x, float y)
{
	sf::Sprite& tr = get<sf::Sprite>(id);
	tr.move(x, y);
}

////////////////////////////////////////////////////////////
const sf::Vector2f& GraphicManager::getPosition(size_t id)
{
	sf::Sprite& tr = get<sf::Sprite>(id);
	return tr.getPosition();
}

////////////////////////////////////////////////////////////
float GraphicManager::getRotation(size_t id)
{
	sf::Sprite& tr = get<sf::Sprite>(id);
	return tr.getRotation();
}

////////////////////////////////////////////////////////////
void GraphicManager::setOrigin(size_t id, float x, float y)
{
	sf::Sprite& tr = get<sf::Sprite>(id);
	tr.setOrigin(x, y);
}

////////////////////////////////////////////////////////////
void GraphicManager::setRotation(size_t id, float angle)
{
	sf::Sprite& tr = get<sf::Sprite>(id);
	tr.setRotation(angle);
}

////////////////////////////////////////////////////////////
void GraphicManager::setScale(size_t id, float x, float y)
{
	sf::Sprite& tr = get<sf::Sprite>(id);
	tr.setScale(x, y);
}

////////////////////////////////////////////////////////////
void GraphicManager::setTexture(size_t id, size_t texture_id)
{
	sf::Sprite& sprite = get<sf::Sprite>(id);
	sf::Texture& texture = ResourceManager::get<sf::Texture>(texture_id);
	sprite.setTexture(texture, true);
}

////////////////////////////////////////////////////////////
float GraphicManager::getSpriteWidth(size_t id)
{
	sf::Sprite& sprite = get<sf::Sprite>(id);
	return sprite.getLocalBounds().width;
}

////////////////////////////////////////////////////////////
float GraphicManager::getSpriteHeight(size_t id)
{
	sf::Sprite& sprite = get<sf::Sprite>(id);
	return sprite.getLocalBounds().height;
}

////////////////////////////////////////////////////////////
void GraphicManager::center(size_t id)
{
	setOrigin(id, getSpriteWidth(id)/2, getSpriteHeight(id)/2);
}

}