////////////////////////////////////////////////////////////
// Zeus Game Engine
// Copyright (C) Excellium (excellium at outlook dot com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
////////////////////////////////////////////////////////////

#include <ZGE/WidgetManager.hpp>
#include <ZGE/GraphicManager.hpp>

namespace ZGE
{

////////////////////////////////////////////////////////////
Widget::Widget() :
m_graph_id(GraphicManager::create<Graph>())
{
}

////////////////////////////////////////////////////////////
Widget::~Widget()
{
	for( auto& parent : m_parents )
		detach(*parent);
}

////////////////////////////////////////////////////////////
Widget& Widget::init()
{
	m_focused_child = 0;
	onInit();

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::attach(Widget& widget)
{
	if( m_children.empty() )
		widget.focus();

	m_children.push_back(&widget);
	widget.m_parents.push_back(this);
	GraphicManager::get<Graph>(m_graph_id).attach(widget.m_graph_id);

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::detach(Widget& widget)
{
	for( auto it = m_children.begin(); it != m_children.end(); ++it )
		if( *it == &widget )
		{
			m_children.erase(it);

			if( m_focused_child >= m_children.size() )
				m_focused_child = 0;

			break;
		}

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::show()
{
	m_visible = true;
	GraphicManager::attach(m_graph_id);

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::hide()
{
	m_visible = false;
	GraphicManager::detach(m_graph_id);

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::lock()
{
	m_locked = true;
	onLock();

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::unlock()
{
	m_locked = false;
	onUnlock();

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::activate()
{
	show();
	unlock();

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::deactivate()
{
	hide();
	lock();

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::focus()
{
	m_focused = true;
	onFocus();

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::unfocus()
{
	m_locked = false;
	onUnfocus();

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::setPosition(const sf::Vector2f& pos)
{
	return move(pos - m_pos);
}

////////////////////////////////////////////////////////////
Widget& Widget::setPosition(float pos_x, float pos_y)
{
	return setPosition({pos_x, pos_y});
}

////////////////////////////////////////////////////////////
Widget& Widget::move(const sf::Vector2f& offset)
{
	GraphicManager::get<Graph>(m_graph_id).move(offset);

	//for( auto& child : m_children )
	//	child->move(offset);

	m_pos+=offset;

	onMove();

	return *this;
}

////////////////////////////////////////////////////////////
Widget& Widget::move(float offset_x, float offset_y)
{
	return move({offset_x, offset_y});
}

////////////////////////////////////////////////////////////
Widget& Widget::align(const sf::FloatRect& limits, Alignment alignment)
{
	sf::Vector2f pos;

	switch( alignment )
	{
		case LeftTop      : pos = {limits.left, 							  limits.top}; 								  break;
		case LeftMiddle   : pos = {limits.left, 							  limits.top + limits.height/2 - m_size.y/2}; break;
		case LeftBottom   : pos = {limits.left, 							  limits.top + limits.height   - m_size.y};   break;
		case MiddleTop    : pos = {limits.left + limits.width/2 - m_size.x/2, limits.top}; 								  break;
		case MiddleMiddle : pos = {limits.left + limits.width/2 - m_size.x/2, limits.top + limits.height/2 - m_size.y/2}; break;
		case MiddleBottom : pos = {limits.left + limits.width/2 - m_size.x/2, limits.top + limits.height   - m_size.y};   break;
		case RightTop     : pos = {limits.left + limits.width   - m_size.x,   limits.top}; 								  break;
		case RightMiddle  : pos = {limits.left + limits.width   - m_size.x,   limits.top + limits.height/2 - m_size.y/2}; break;
		case RightBottom  : pos = {limits.left + limits.width   - m_size.x,   limits.top + limits.height   - m_size.y};   break;
	}

	return setPosition(pos);
}

////////////////////////////////////////////////////////////
Widget& Widget::setSize(const sf::Vector2f& size)
{
	m_size = size;
	onResizing();

	return *this;
}

////////////////////////////////////////////////////////////
const sf::Vector2f& Widget::getSize()
{
	return m_size;
}

////////////////////////////////////////////////////////////
void Widget::focusPrevious()
{
	if( m_focused_child > 0 )
	{
		m_children[m_focused_child]->unfocus();
		m_focused_child--;
		m_children[m_focused_child]->focus();
	}

	return *this;
}

////////////////////////////////////////////////////////////
void Widget::focusNext()
{
	if( m_focused_child < m_children.size() - 1 )
	{
		m_children[m_focused_child]->unfocus();
		m_focused_child++;
		m_children[m_focused_child]->focus();
	}

	return *this;
}

////////////////////////////////////////////////////////////
MenuItem::MenuItem(const sf::String& label) :
m_label_id(GraphicManager::createText())
{
	sf::Text& text = GraphicManager::get<sf::Text>(m_label_id);
	text.setString(label);
	text.setCharacterSize(40);
	//text.setOrigin(text.getLocalBounds().width/2, text.getLocalBounds().height/2);
	setSize({text.getLocalBounds().width, text.getLocalBounds().height});
	GraphicManager::get<Graph>(m_graph_id).attach(m_label_id);
}

////////////////////////////////////////////////////////////
const sf::Vector2f& MenuItem::getSize()
{
	sf::Text& text = GraphicManager::get<sf::Text>(m_label_id);
	return {text.getLocalBounds().width, text.getLocalBounds().height};
}

////////////////////////////////////////////////////////////
void MenuItem::onFocus()
{
	sf::Text& text = GraphicManager::get<sf::Text>(m_label_id);
	text.setColor(sf::Color::Red);
}

////////////////////////////////////////////////////////////
void MenuItem::onUnfocus()
{
	sf::Text& text = GraphicManager::get<sf::Text>(m_label_id);
	text.setColor(sf::Color::White);
}

////////////////////////////////////////////////////////////
MenuItemValidation::MenuItemValidation(const sf::String& label) :
MenuItem(label),
m_return_key_pressed_id(EventManager::create<KeyPressed>(sf::Keyboard::Return))
{
	EventManager::connect(m_return_key_pressed_id, this, &MenuItemValidation::onValidation);
}

////////////////////////////////////////////////////////////
MenuItemValidation& MenuItemValidation::connect(std::function<void()> slot)
{
    m_validationSignal.connect(slot);
    return *this;
}

////////////////////////////////////////////////////////////
void MenuItemValidation::onValidation()
{
	m_validationSignal();
}

////////////////////////////////////////////////////////////
void MenuItemValidation::onFocus()
{
	MenuItem::onFocus();
	EventManager::attach(m_return_key_pressed_id);
}

////////////////////////////////////////////////////////////
void MenuItemValidation::onUnfocus()
{
	MenuItem::onUnfocus();
	EventManager::detach(m_return_key_pressed_id);
}

////////////////////////////////////////////////////////////
MenuItemSelection::MenuItemSelection(const sf::String& label, const sf::String& selection) :
MenuItem(label)
{

}

////////////////////////////////////////////////////////////
MenuItemEdition::MenuItemEdition(const sf::String& label, const sf::String& def) :
MenuItem(label),
m_text_entered_event(EventManager::create<TextEntered>()),
m_text_id(GraphicManager::createText())
{
	sf::Text& label_ = GraphicManager::get<sf::Text>(m_label_id);
	float y = label_.getLocalBounds().top;
	float x = label_.getLocalBounds().left + label_.getLocalBounds().width/2;

	sf::Text& text = GraphicManager::get<sf::Text>(m_text_id);
	text.setPosition(x, y);

	EventManager::connect(m_text_entered_event, this, &MenuItemEdition::onTextEntered);
}

////////////////////////////////////////////////////////////
MenuItemEdition::~MenuItemEdition()
{
	EventManager::erase(m_text_entered_event);
	GraphicManager::erase(m_text_id);
}

////////////////////////////////////////////////////////////
MenuItemEdition& MenuItemEdition::connect(std::function<void()> function)
{
    m_edition_signal.connect(function);
    return *this;
}

////////////////////////////////////////////////////////////
void MenuItemEdition::onTextEntered(size_t unicode)
{
	sf::Text& text = GraphicManager::get<sf::Text>(m_text_id);
	text.setString(text.getString() + unicode);
}

////////////////////////////////////////////////////////////
void MenuItemEdition::onFocus()
{
	MenuItem::onFocus();
	EventManager::attach(m_text_entered_event, true);
}

////////////////////////////////////////////////////////////
void MenuItemEdition::onUnfocus()
{
	MenuItem::onUnfocus();
	EventManager::detach(m_text_entered_event);
}

////////////////////////////////////////////////////////////
Menu::Menu(const sf::String& title, Alignment alignment, float line_spacing) :
m_up_key_pressed_event(EventManager::create<KeyPressed>(sf::Keyboard::Up)),
m_down_key_pressed_event(EventManager::create<KeyPressed>(sf::Keyboard::Down)),
m_back_shape(GraphicManager::create<sf::RectangleShape>()),
m_alignment(alignment),
m_line_spacing(line_spacing)
{
	EventManager::connect(m_up_key_pressed_event,   this, &Menu::focusPrevious);
	EventManager::connect(m_down_key_pressed_event, this, &Menu::focusNext);

	sf::RectangleShape& back_shape = GraphicManager::get<sf::RectangleShape>(m_back_shape);
	back_shape.setPosition(m_pos);
	back_shape.setFillColor(sf::Color::Green);
	GraphicManager::get<Graph>(m_graph_id).attach(m_back_shape);
}

////////////////////////////////////////////////////////////
Menu::~Menu()
{
	EventManager::erase(m_up_key_pressed_event);
	EventManager::erase(m_down_key_pressed_event);
	GraphicManager::erase(m_back_shape);
}

////////////////////////////////////////////////////////////
Menu& Menu::attach(size_t slot_id)
{
	MenuItem* new_slot = &WidgetManager::get<MenuItem>(slot_id);
	m_slots.push_back(new_slot);

	float offset = 0;

	for( auto& slot : m_slots )
	{
		slot->setPosition(0, offset);
		sf::Vector2f size = slot->getSize();

		offset+=m_line_spacing;
		offset+=size.y;

		if( size.x >= m_size.x )
			m_size.x = size.x;
	}

	m_size.y = offset;

	sf::RectangleShape& back_shape = GraphicManager::get<sf::RectangleShape>(m_back_shape);
	back_shape.setSize(m_size);

	Widget::attach(*new_slot);

	for( auto& slot : m_slots )
	{
		//slot->setSize();
		//slot->align({, , m_size.x, }, offset);
	}

	init();

	return *this;
}

////////////////////////////////////////////////////////////
void Menu::onInit()
{
	if( !m_slots.empty() )
		m_slots.front()->focus();
}

////////////////////////////////////////////////////////////
void Menu::onLock()
{
	EventManager::detach(m_up_key_pressed_event);
	EventManager::detach(m_down_key_pressed_event);

	for( size_t k(0); k < m_slots.size(); k++ )
		m_slots[k]->unfocus();
}

////////////////////////////////////////////////////////////
void Menu::onUnlock()
{
	EventManager::attach(m_up_key_pressed_event, true);
	EventManager::attach(m_down_key_pressed_event, true);

	init();
}

////////////////////////////////////////////////////////////
std::map<size_t, Widget::ptr> WidgetManager::m_widgets;
std::vector<Widget::ptr> WidgetManager::m_attached_widgets;

////////////////////////////////////////////////////////////
void WidgetManager::clear()
{
    for( auto& pair : m_widgets )
        delete pair.second;
}

////////////////////////////////////////////////////////////
bool WidgetManager::erase(size_t id)
{
    auto it = m_widgets.find(id);
    if( it == m_widgets.end() )
        return false;
    delete it->second;
    m_widgets.erase(it);
    return true;
}

////////////////////////////////////////////////////////////
void WidgetManager::attach(size_t id)
{
	Widget& widget = get<Widget>(id);

	for( auto it = m_attached_widgets.begin(); it != m_attached_widgets.end(); ++it )
		if( *it == &widget )
			return;

	GraphicManager::attach(widget.m_graph_id);
	widget.activate();
	m_attached_widgets.push_back(&widget);
}

////////////////////////////////////////////////////////////
void WidgetManager::detach(size_t id)
{
	Widget& widget = get<Widget>(id);

	for( auto it = m_attached_widgets.begin(); it != m_attached_widgets.end(); ++it )
		if( *it == &widget )
		{
			m_attached_widgets.erase(it);
			widget.deactivate();
			break;
		}
}

}