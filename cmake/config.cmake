cmake_minimum_required(VERSION 2.8)
set(CMAKE_MODULE_PATH ${ROOTDIR}/cmake/modules)
set(EXECUTABLE_OUTPUT_PATH ${ROOTDIR}/examples/bin)
set(LIBRARY_OUTPUT_PATH ${ROOTDIR}/lib)
find_package(SFML REQUIRED system audio window graphics network)
